<?php

if (!user_access('forum_them_edit') && $user['id'] != $theme->id_user) {
    header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
    exit;
} else {
    if (isset($_POST['save'])) {
        $name = mysql_real_escape_string(trim($_POST['name']));
        $description = mysql_real_escape_string(trim($_POST['description']));
        $type = ($user['group_access'] > 7 && isset($_POST['type'])) ? 1 : 0;
        $razdel_id = abs(intval($_POST['razdel']));
        $forum_id = mysql_fetch_object(mysql_query('SELECT `id_forum` FROM `forum_razdels` WHERE `id` = '.$razdel_id))->id_forum;
        if (preg_match("/[^(\w)|(\x7F-\xFF)|(\s)|(\,\.\-)]/", $_POST['name'], $m)) {
           ?>
            <div class = 'err'>
                В поле &laquo;Название темы&raquo; присутствуют запрещенные <span style="font-weight: bold; color: red;"><?= $m[0]?></span> символы!.
            </div>
            <?            
        } else         
        if (mb_strlen($name) < $set['forum_new_them_min_i'] || mb_strlen($name) > $set['forum_new_them_max_i']) {
            ?>
            <div class = 'err'>В поле "Название темы" можно использовать от <?= $set['forum_new_them_min_i'] ?> до <?= $set['forum_new_them_max_i'] ?> символов.</div>
            <?
        } elseif (mb_strlen($description) < 3) {
            ?>
            <div class = 'err'>Слишком короткое содержание темы.</div>
            <?
        } else {
            $_SESSION['success'] = '<div class = "msg">Тема успешно изменена.</div>';
            mysql_query('UPDATE `forum_themes` SET `id_forum` = '.$forum_id.', `id_razdel` = '.$razdel_id.', `id_admin` = '.$user['id'].', `name` = "'.$name.'", `description` = "'.$description.'", `type` = '.$type.', `time_edit` = "'.$time.'" WHERE `id` = '.$theme->id);
            header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
            exit;
        }
    } elseif (isset($_POST['delete']) && (user_access('forum_them_del'))) {
        admin_log('Форум', 'Темы', 'Тема "'.$theme->name.'" была удалена из раздела "'.$razdel->name.'".');
        $files = mysql_query('SELECT `name` FROM `forum_post_files` WHERE `id_theme` = '.$theme->id);
        while ($file = mysql_fetch_object($files)) {
            unlink(H.FORUM.'/files/'.$file->name);
        }
        $votes = mysql_query('SELECT `id` FROM `forum_votes` WHERE `id_theme` = '.$theme->id);
        while ($vote = mysql_query($votes)) {
            mysql_query('DELETE FROM `forum_vote_voices` WHERE `id_vote` = '.$vote->id);
        }
        mysql_query('DELETE FROM `forum_votes` WHERE `id_theme` = '.$theme->id);
        mysql_query('DELETE FROM `forum_post_rating` WHERE `id_theme` = '.$theme->id);
        mysql_query('DELETE FROM `forum_posts` WHERE `id_theme` = '.$theme->id);
        mysql_query('DELETE FROM `forum_post_files` WHERE `id_theme` = '.$theme->id);
        mysql_query('DELETE FROM `forum_votes_var` WHERE `id_theme` = '.$theme->id);
        mysql_query('DELETE FROM `forum_themes` WHERE `id` = '.$theme->id);
        $_SESSION['msg'] = '<div class = "msg">Тема успешно удалена.</div>';
        header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/');
        exit;
    }
    ?>
    <form action = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/edit_theme.html' method = 'post' class="p_m">
        <b>Название темы (<?= $set['forum_new_them_max_i'] ?> символ(а/ов)):</b><br />
        <input type = 'text' name = 'name' value = '<?= $theme->name ?>' style = 'width: 96%' /><br /><br />
        <b>Содержание темы:</b><br />
        <textarea name = 'description' style = 'width: 96%'><?= $theme->description ?></textarea><br /><br />
        <b>Категория:</b><br />
        <select name = 'razdel'>
            <?
            $forums = mysql_query('SELECT `id`, `name`, `access` FROM `forum`');
            while ($forum_s = mysql_fetch_object($forums)) {
                if ($forum_s->access == 0 || ($forum_s->access == 1 && $user['group_access'] > 7) || ($forum_s->access == 2 && $user['group_access'] > 2)) {
                    ?>
                    <option disabled = 'disabled'><b><?= $forum_s->name ?></b></option>
                    <?
                    $razdels = mysql_query('SELECT `id`, `name` FROM `forum_razdels` WHERE `id_forum` = '.$forum_s->id);
                    while ($cat = mysql_fetch_object($razdels)) {
                        ?>
                        <option value = '<?= $cat->id ?>' <?= ($cat->id == $theme->id_razdel) ? 'selected = "selected"' : NULL ?>>>> <?= $cat->name ?></option>
                        <?
                    }
                } else {
                    continue;
                }
            }
            ?>
        </select><br /><br />
        <?
        if ($user['group_access'] > 7) {
            ?>
            <b>Вывод темы в разделе:</b><br />
            <label><input type = 'checkbox' name = 'type' value = '1' <?= ($theme->type == 1) ? 'checked = "checked"' : NULL ?> /> Тема всегда вверху</label><br /><br />
            <?
        }
        ?>
        <input type = 'submit' name = 'save' value = 'Сохранить' /> 
        <?
        if (user_access('forum_them_del')) {
            ?>
            <input type = 'submit' name = 'delete' value = 'Удалить тему' />
            <?
        }
        ?>
    </form>
    <div class = 'p_m' style = 'text-align: right'><a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>.html'>Отменить редактирование</a></div>
    <?
    include_once '../sys/inc/tfoot.php';
    exit;
}

?>