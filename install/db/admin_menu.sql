-- phpMyAdmin SQL Dump
-- version 4.3.4
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Июл 03 2015 г., 03:30
-- Версия сервера: 5.6.22-log
-- Версия PHP: 5.5.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `test_local`
--

-- --------------------------------------------------------

--
-- Структура таблицы `admin_menu`
--

CREATE TABLE IF NOT EXISTS `admin_menu` (
  `id` int(11) NOT NULL,
  `type` enum('link','razd') NOT NULL DEFAULT 'link',
  `name` varchar(99) NOT NULL,
  `url` varchar(99) NOT NULL,
  `counter` varchar(99) NOT NULL,
  `pos` int(11) NOT NULL DEFAULT '0',
  `img` varchar(99) DEFAULT 'default.png',
  `accesses` varchar(99) DEFAULT NULL,
  `time` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COMMENT='Админ миню';

--
-- Дамп данных таблицы `admin_menu`
--

INSERT DELAYED INTO `admin_menu` (`id`, `type`, `name`, `url`, `counter`, `pos`, `img`, `accesses`, `time`) VALUES
(1, 'link', 'Администрация', 'administration.php', 'admins.php', 1, '4.png', 'adm_show_adm', 1400723795),
(2, 'razd', 'Модули сайта', '', '', 2, 'default.png', '', 1400723795),
(3, 'link', 'Модули', 'modules.php', 'modules.php', 3, 'modules.png', 'modules', 1400723795),
(4, 'link', 'Установка Модулей', 'modules_install.php', '', 4, 'modules_install.png', 'modules', 1400723795),
(5, 'link', 'Fiera Market', 'modules_market.php', 'modules_new.php', 5, 'fiera_market.png', 'modules_install', 1400723795),
(6, 'razd', 'Админ раздел', '', '', 6, '', '', 1400723795),
(7, 'link', 'Список забаненных', 'banlist.php', 'banlist.php', 7, '14.png', 'adm_banlist', 1400723795),
(8, 'razd', 'Информация', '', '', 8, '', '', 1400723795),
(9, 'link', 'Общая информация', 'info_a.php', '', 9, 'info.png', 'adm_mysql', 1400723795),
(10, 'link', 'Статистика сайта', 'statistic.php', '', 10, '3.png', 'adm_statistic', 1400723795),
(11, 'link', 'Действия администрации', 'adm_log.php', '', 11, '5.png', 'adm_log_read', 1400723795),
(12, 'link', 'Добавить смайлы', '/pages/smiles/?type=add_smile', 'smiles.php', 12, '28.png', 'smiles', 1400723795),
(13, 'link', 'Управление новостями', 'news.php', '', 13, '8.png', 'adm_news', 1400723795),
(14, 'link', 'Управление рекламой', 'rekl.php', '', 14, '7.png', 'adm_rekl', 1400723795),
(15, 'link', 'Управление главным меню', 'menu.php', '', 15, '6.png', 'adm_menu', 1400723795),
(16, 'razd', 'Настройки', '', '', 16, '', '', 1400723795),
(17, 'link', 'Настройки системы', 'settings_sys.php', '', 17, '9.png', 'adm_set_sys', 1400723795),
(18, 'link', 'Настройки BBcode', 'settings_bbcode.php', '', 18, '10.png', 'adm_set_sys', 1400723795),
(19, 'link', 'Настройки форума', 'settings_forum.php', '', 19, '10.png', 'adm_set_forum', 1400723795),
(20, 'link', 'Пользовательские настройки', 'settings_user.php', '', 20, '12.png', 'adm_set_user', 1400723795),
(21, 'link', 'Настройки привилегии', 'accesses.php', '', 21, '13.png', 'adm_accesses', 1400723795),
(22, 'link', 'Настройки изображений', 'settings_foto.php', '', 22, '18.png', 'adm_set_foto', 1400723795),
(24, 'link', 'Рефералы сайта', 'referals.php', '', 23, '21.png', 'adm_ref', 1400723795),
(25, 'link', 'Редактирование IP операторов', 'opsos.php', '', 24, '22.png', 'adm_ip_edit', 1400723795),
(26, 'link', 'Бан по IP адресу - диапазону', 'ban_ip.php', '', 25, '23.png', 'adm_ban_ip', 1400723795),
(27, 'link', 'Настройки кэша', 'cache.php', '', 26, '27.png', 'cache_set', 1400723795),
(28, 'link', 'MySQL запросы', 'mysql.php', '', 27, '24.png', 'adm_mysql', 1400723795),
(29, 'link', 'Заливка таблиц mysql', 'tables.php', '', 28, '25.png', 'adm_mysql', 1400723795),
(30, 'razd', 'Темы оформления', '', '', 29, '', '', 1400723795),
(31, 'link', 'Список тем оформления', 'themes.php', '', 30, '26.png', 'adm_themes', 1400723795),
(40, 'link', 'mailGuard - Защитник почты', 'mailGuard.php', 'mailGuard.php', 31, 'mailGuard.png', 'mailGuard', 1404260134),
(39, 'link', 'Планировщик задач - Cron', 'cron.php', 'cron.php', 32, '29.png', 'cron_system', 1401923690),
(41, 'link', 'Системный журнал', 'jurnal_system.php', 'jurnal_system.php', 33, 'jurnal_s.png', 'system_jurnal', 1404517822),
(42, 'link', 'Права доступа - cmod', 'cmod.php', '', 34, 'cmod.png', 'cmod', 1407875481),
(43, 'link', 'Информация о системе', 'info_system.php', '', 35, 'info_system.png', 'info_system', 1407875603),
(44, 'link', 'Управление пунктами профиля', 'info_edit.php', '', 36, 'profile_edit.png', 'edit_info_menu', 1409066915),
(45, 'link', 'Лицензия Fiera', 'license.php', 'license.php', 37, 'fieral.png', 'license', 1410991227),
(46, 'link', 'Служба поддержки', 'support.php', '', 38, 'support.png', 'license_support', 1410997762),
(47, 'link', 'Редактирование Кабинета', 'user_menu.php', '', 39, 'user_menu.png', 'user_cab', 1414737901),
(48, 'link', 'Защита сайта', 'antihack.php', '', 40, 'guard.png', 'all_guard', 1417784527);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `admin_menu`
--
ALTER TABLE `admin_menu`
  ADD PRIMARY KEY (`id`), ADD KEY `pos` (`pos`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `admin_menu`
--
ALTER TABLE `admin_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
